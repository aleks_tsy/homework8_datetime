from datetime import datetime, timedelta


def get_birthdays_per_week(dict):
    today = datetime.now()
    weekly_dict = {}
    result_dict = {"Monday":"", "Tuesday":"", "Wednesday":"", "Thursday":"", "Friday":""}

    for keys, values in dict.items():
        values = values.replace(year=today.year)
        if today + timedelta(days=7) >= values > today:
            weekly_dict[values] = keys
    for keys, values in weekly_dict.items():

        if keys.weekday() == 0 or keys.weekday() == 5 or keys.weekday() == 6:

            result_dict["Monday"] += weekly_dict[keys] + ", "
        elif keys.weekday() == 1:
            result_dict["Tuesday"] += weekly_dict[keys] + ", "
        elif keys.weekday() == 2:
            result_dict["Wednesday"] += weekly_dict[keys] + ", "
        elif keys.weekday() == 3:
            result_dict["Thursday"] += weekly_dict[keys] + ", "
        elif keys.weekday() == 4:
            result_dict["Friday"] += weekly_dict[keys] + ", "
    for keys, values in result_dict.items():
        result_dict[keys] = corect_birthday_table(values)

    print(
        f"Monday: {result_dict['Monday']}\nTuesday:{result_dict['Tuesday']} \nWednesday:{result_dict['Wednesday']} \nThursday:{result_dict['Thursday']} \nFriday:{result_dict['Friday']}")


def corect_birthday_table(lst_of_names):
    lst_of_names = lst_of_names[:-2]
    return lst_of_names


get_birthdays_per_week({"Jim": datetime(year=1996, month=1, day=19), "Aleks": datetime(year=2001, month=11, day=21),
                        "Alla": datetime(year=2001, month=8, day=6), 'Misha': datetime(year=2001, month=7, day=31)})
